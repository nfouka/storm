<?php

namespace tests ; 

use Controller\SomeClass;
use PHPUnit\Framework\TestCase;


class StubTest extends TestCase
{
    public function testStub()
    {

        $stub = $this->getMockBuilder(SomeClass::class)
        ->disableOriginalConstructor()
        ->disableOriginalClone()
        ->disableArgumentCloning()
        ->disallowMockingUnknownTypes()
        ->getMock();
        
        // Configurer le bouchon.
        $stub->method('doSomething')
        ->willReturn('foo');
        
        // Appeler $stub->doSomething() retournera désormais
        // 'foo'.
        $this->assertSame('foo', $stub->doSomething());
    }
    
    public function testReturnArgumentStub()
    {
        // Créer un bouchon pour la classe SomeClass.
        $stub = $this->createMock(SomeClass::class);
        
        // Configurer le bouchon.
        $stub->method('doSomething')
        ->will($this->returnArgument(0));
        
        // $stub->doSomething('foo') retourn 'foo'
        $this->assertSame('foo', $stub->doSomething('foo'));
        
        // $stub->doSomething('bar') returns 'bar'
        $this->assertSame('bar', $stub->doSomething('bar'));
    }
    
    
    public function testReturnSelf()
    {
        // Créer un bouchon pour la classe SomeClass.
        $stub = $this->createMock(SomeClass::class);
        
        // Configurer le bouchon.
        $stub->method('doSomething')
        ->will($this->returnSelf());
        
        // $stub->doSomething() retourne $stub
        $this->assertSame($stub, $stub->doSomething());
    }
    
    
    
    public function testOnConsecutiveCallsStub()
    {
        // Créer un bouchon pour la classe SomeClass.
        $stub = $this->createMock(SomeClass::class);
        
        // Configurer le bouchon.
        $stub->method('doSomething')
        ->will($this->onConsecutiveCalls(2, 3, 5, 7));
        
        
        // $stub->doSomething() retourne une valeur différente à chaque fois
        $this->assertSame(2, $stub->doSomething());
        $this->assertSame(3, $stub->doSomething());
        $this->assertSame(51, $stub->doSomething());
    }
    
    

    public function test2Stub()
    {
        // Créer un bouchon pour la classe SomeClass.
        $stub = $this->createMock(SomeClass::class);
        
        // Configurer le bouchon.
        $stub->method('doSomething')
        ->willReturn('foo');
        
        // Appeler $stub->doSomething() va maintenant retourner
        // 'foo'.
        $this->assertSame('foo', $stub->doSomething());
    }
    
    
}